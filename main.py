import webapp2
from app import routes

app = webapp2.WSGIApplication(routes.list, debug=True)