This is a prototype of a simple web application that allows multiple users to submit reviews for hotels they may have visited as well as see reviews submitted by other users. Each review consists of a title, a short comment and a numeric rating system, on the scale of 1 to 5, presented as a number of gold stars. Users can add hotels by submitting the name of the hotel, a short description and an image of the hotel. The main features of the application are shown in Figure 1 and are as follows. The application

1.	Allows users to view previously reviewed hotels
2.	Allows users to view reviews for hotels that have been added
3.	Allows users to add new hotels
4.	Allows users to submit reviews for hotels
5.	Allows users to delete any submitted reviews
6.	Allows users to sort reviews by date or rating
7.	Allows users to view the cumulative numeric score for each hotel as well as its average rating based on rating across all reviews
8.	Validates user input when creating hotels and submitting reviews

It was built in Python using the webapp2 web development framework and hosted on the Google App Engine.


Third Party Resources
- Bootstrap CSS Framework
Bootstrap is released under the Apache 2 license and is copyright 2013 Twitter. , which allows free download and use of Bootstrap, in whole or in part, for personal, company internal or commercial purposes as well as the use of Bootstrap in packages or distributions created.
- jQuery Library
Licensed under the MIT License to The jQuery Foundation which makes it free to use in any project as long as the copyright header is left intact.
- jQuery Validation Plugin
Also licensed under the MIT License to Jörn Zaefferer .

