from google.appengine.ext import db


""" Models a single hotel resource with name, description, dates created and
    reviewed, associated image and id of user who created it

"""
class Hotel(db.Model):
    # Basic hotel details
    name = db.StringProperty(required=True)
    description = db.StringProperty(multiline=True)
    created_at = db.DateTimeProperty(auto_now_add=True)
    last_reviewed_at = db.DateTimeProperty()
    user_id = db.StringProperty(required=True) # ID of user who created hotel

    # Fields for associated image
    image_id = db.StringProperty(multiline=True, default='')
    image_data = db.BlobProperty()

    """ Returns sum of ratings for all reviews of instance """
    def cumulative_rating(self):
        sum = 0
        for review in self.reviews:
            sum += review.rating
        return sum

    """ Returns average of rating scores for all reviews of instance """
    def average_rating(self):
        count = self.reviews.count()
        if count != 0: return self.cumulative_rating() / count
        return 0



""" Models a single hotel review with hotel it belongs to, title,
    rating, name, user who made it, any additional comments and
    the date it was created

"""
class Review(db.Model):
    hotel  = db.ReferenceProperty(Hotel, collection_name='reviews', required=True)
    title  = db.StringProperty()
    rating = db.IntegerProperty(required=True, choices=(1,2,3,4,5))
    comments = db.StringProperty(multiline=True)
    created_at = db.DateTimeProperty(auto_now_add=True)
    user_id = db.StringProperty(required=True) # ID of user who posted review
