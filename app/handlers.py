import os
import webapp2
import jinja2
import re
import datetime

from google.appengine.ext import db
from google.appengine.api import users

from app import models


# Initialize Jinja Environment
env = jinja2.Environment(
        loader=jinja2.FileSystemLoader('templates'),
        extensions=['jinja2.ext.autoescape'],
        autoescape=True,
        )
# env.globals['uri_for'] = webapp2.uri_for

# TODO: user authentication

class HotelsHandler(webapp2.RequestHandler):
    """ Handler for operations on a collection of hotel resources

    """

    def get(self, action=None):
        """ Performs 'GET'-based actions on a collection of hotel resources
            based on action supplied in url. Displays list of
            hotels if no action supplied or form to create new hotel if
            'new' action supplied in URL.
        """
        context = {}
        if action == 'new':
            template = env.get_template('hotels/new.html') # display new hotel form
        else:
            # Retrieve hotels ordered by review date
            hotels = db.Query(models.Hotel).order('-last_reviewed_at').run()
            context['hotels'] = hotels
            template = env.get_template('hotels/index.html') # display hotels list
        self.response.write(template.render(context))

    def post(self, action=None):
        """ Performs 'POST'-based actions on a collection of hotel resources
            based on action supplied in url. Creates a new hotel resource and
            displays it if no action supplied in URL.
        """
        _name = self.request.get('name')
        _description = self.request.get('description')
        _image = self.request.get('image')

        hotel = models.Hotel(name=_name,
                             description=_description,
                             user_id='1')
        hotel.put()
        hotel_id = hotel.key().id()

        """ Ensure attempt to save image only proceeds if image data
            is present and in valid type (String)
            Use hotel id returned on save as image's identifier
        """
        valid_image_data = isinstance(_image, str)
        if valid_image_data:
            hotel.image_data = _image
            hotel.image_id = str(hotel_id)
            hotel.put()
        self.redirect_to('show-hotel', hotel_id=hotel_id)


class HotelHandler(webapp2.RequestHandler):
    """ Handler for operations on a single hotel resource """

    def get(self, hotel_id, action=None):
        """ Performs 'GET'-based actions on a single hotel resource
            based on action supplied in url. Displays details of
            hotel if no action supplied.
        """
        if action == None:
            # show action
            hotel = models.Hotel.get_by_id(long(re.sub(r'\/', '', hotel_id)))
            template = env.get_template('hotels/show.html') # display hotel details
            context = { 'hotel': hotel }
            self.response.write(template.render(context))
        else:
            # handle edit, other custom actions
            context = {}
            template = env.get_template('hotels/edit.html') # TODO: Add edit template
            self.response.write(template.render(context))

    def post(self, hotel_id):
        self.response.write('hotel post action')

    def delete(self, hotel_id):
        self.response.write('hotel delete action')

    def put(self, hotel_id):
        self.response.write('hotel put action')


class ReviewsHandler(webapp2.RequestHandler):
    """ Handler for actions on a collection of review resources. If hotel_id
        parameter is present, operations are restricted to hotel specified by
        hotel_id.

    """

    def get(self, hotel_id, action=None):
        """ Performs 'GET'-based actions on a collection of review resources
            based on action supplied in url. Displays list of
            reviews if no action supplied or form to create a new review if
            'new' action supplied in URL.
        """

        """ Determine sort order from 'order' parameter sent with request.
            Defaults to ordering by date created.
        """
        _order = self.request.get('order')
        if _order.strip() == '':
          order = '-created_at'
        else:
          order = re.sub(r'date', 'created_at', _order).strip()

        if hotel_id.strip() == '':
            # # get all reviews
            reviews = db.Query(models.Review).order(order).run()
            context = { 'reviews': reviews, 'sort_url': '/reviews?order=' }
            template = env.get_template('reviews/index.html')
        else:
            hotel = models.Hotel.get_by_id(long(re.sub(r'\/', '', hotel_id)))
            context = { 'hotel': hotel }
            if action == 'new':
                # new review
                context['hotel_id'] = hotel_id
                template = env.get_template('reviews/new.html')
            else:
                # get all reviews for specified hotel
                context['reviews'] = hotel.reviews.order(order)
                context['sort_url'] = '/hotels/%s/reviews?order=' % hotel_id
                template = env.get_template('reviews/index.html')
        self.response.write(template.render(context))

    def post(self, hotel_id):
        """ Performs 'POST'-based actions on a collection of review resources.
            Currently only creates a new review resource and displays it.
        """
        hotel = models.Hotel.get_by_id(long(re.sub(r'\/', '', hotel_id)))
        title = self.request.get('title')
        rating = int(self.request.get('rating')[:1])
        comments = self.request.get('comments')

        review = models.Review(hotel=hotel,
                               title=title,
                               rating=rating,
                               comments=comments,
                               user_id='1')
        review.put()

        # Update associated hotel's date of last review field
        hotel.last_reviewed_at = datetime.datetime.now()
        hotel.put()

        # Display created review
        self.redirect_to('hotel-show-review',
                         hotel_id=hotel.key().id(),
                         review_id=review.key().id())


class ReviewHandler(webapp2.RequestHandler):
    """ Handler for operations on a single review resource. The
        'hotel_id' parameter can be used to specify hotel the review belowngs to.
        by hotel_id.

    """

    def get(self, review_id, hotel_id=None, action=None):
      """ Performs 'GET'-based actions on a single hotel resource
          based on action supplied in url. Displays details of
          hotel if no action supplied.
      """
      if action == None:
          review = models.Review.get_by_id(long(re.sub(r'\/', '', review_id)))
          context = { 'review': review }
          template = env.get_template('reviews/show.html')
      else:
          """ TODO:  Implement edit and other operations on single review"""
          self.response.write('TODO: review edit, other instance actions')
      self.response.write(template.render(context))


    def delete(self, review_id, hotel_id=None):
        """ Performs 'DELETE'-based operations on a single review resource.
            Currently only deletes specified review resource from datastore.
        """
        review = models.Review.get_by_id(long(re.sub(r'\/', '', review_id)))
        if review == None:
            self.response.write('Review not found!')
        else:
            hotel = review.hotel
            review.delete()

            # NB: Redirect is explicity handled in the template.
            #     The operation returns url for associated hotel as text. The template
            #     then redirects to this url via ajax call.
            destination_url = self.uri_for('show-hotel', hotel_id=hotel.key().id())
            self.response.write(destination_url)
        #self.redirect_to('show-hotel', hotel_id=hotel.key().id())



class ImageHandler(webapp2.RequestHandler):
    """Handler to serve hotel images stored as blobs.
       Simply retrieves blob data and outputs it as an image.
    """

    def get(self, image_id):
        hotel = models.Hotel.get_by_id(long(image_id))
        if hotel is not None:
            self.response.headers['Content-Type'] = 'images/png'
            self.response.out.write(hotel.image_data)
        else:
            self.response.write("Image not found!")
