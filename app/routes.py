import webapp2
from app import handlers

# List of application routes
list = [
        # Access image by identifier eg. '/images/123455'
        webapp2.Route(r'/images/<image_id>',
                      handler=handlers.ImageHandler),
        # Access reviews for a specifc hotel eg. '/hotels/12/reviews'
        webapp2.Route(r'/hotels/<hotel_id>/reviews<:/?>',
                      handler=handlers.ReviewsHandler, name='hotel-list-reviews'),
        # Access operations for collection of reviews of a specific hotel eg. '/hotels/12/reviews/new'
        webapp2.Route(r'/hotels/<hotel_id>/reviews/<action:(new)>',
                      handler=handlers.ReviewsHandler, name='hotel-action-reviews'),
        # Access operations for single review of a specific hotel
        # eg. '/hotels/12/reviews/12/edit'
        webapp2.Route(r'/hotels/<hotel_id>/reviews/<review_id>/<action:(edit)>',
                      handler=handlers.ReviewHandler, name='hotel-action-review'),
        # Access a single review of a specific hotel
        # eg. '/hotels/12/reviews/12'
        webapp2.Route(r'/hotels/<hotel_id>/reviews/<review_id:.*/?>',
                      handler=handlers.ReviewHandler, name='hotel-show-review'),
        # Access all reviews
        # eg. '/reviews'
        webapp2.Route(r'/reviews<:/?>',
                      handler=handlers.ReviewsHandler, name='list-reviews'),
        # Access a specific review
        # eg. '/reviews/12'
        webapp2.Route(r'/reviews/<review_id:.*/?>',
                      handler=handlers.ReviewHandler, name='show-review'),
        # Access all hotels
        # eg. '/hotels'
        webapp2.Route(r'/hotels<:/?>',
                      handler=handlers.HotelsHandler, name='list-hotels'),
        # Access operations for all hotels
        # eg. '/hotels/new'
        webapp2.Route(r'/hotels/<action:(new)>',
                      handler=handlers.HotelsHandler, name='action-hotels'),
        # Access operations for specific hotel
        # eg. '/hotels/12/edit'
        webapp2.Route(r'/hotels/<hotel_id>/<action:(edit)>',
                      handler=handlers.HotelHandler, name='action-hotel'),
        # Access a specific hotel
        # eg. '/hotels/12'
        webapp2.Route(r'/hotels/<hotel_id:.*/?>',
                      handler=handlers.HotelHandler, name='show-hotel'),
        # homepage
        webapp2.Route(r'/',
                      handler=handlers.HotelsHandler, name='home'),
        ]
